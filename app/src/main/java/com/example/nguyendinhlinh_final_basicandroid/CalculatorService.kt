package com.example.nguyendinhlinh_final_basicandroid

import android.app.IntentService
import android.content.Intent
import android.util.Log
import kotlin.concurrent.thread

class CalculatorService : IntentService(CalculatorService::class.simpleName) {

    init {
        Log.d("DinhLinh", "Service is running..")
    }


    override fun onHandleIntent(intent: Intent?) {
        thread(true) {
            calculate()
        }
    }

    fun calculate() {
        var num: Long = 0
        for (i in 1..1000000) {
            num += i
        }
        Thread.sleep(5000)
        sendBroadcast(Intent().apply {
            putExtra("num", num)
            setAction("done")
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("DinhLinh", "Service is killed")
    }

}