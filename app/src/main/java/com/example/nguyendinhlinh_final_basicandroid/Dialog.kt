package com.example.nguyendinhlinh_final_basicandroid

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class Dialog(val num: Long): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let {
            val builder= AlertDialog.Builder(it)
            builder.setTitle("Dialog")
            builder.setMessage("Tong tu 1 den 1000000 la ${num}")
            builder.setNegativeButton("OK"){_, _->
                dismiss()
            }
            builder?.create()
        }?: super.onCreateDialog(savedInstanceState)
    }
}