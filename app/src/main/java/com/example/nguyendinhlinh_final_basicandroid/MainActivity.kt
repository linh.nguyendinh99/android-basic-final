package com.example.nguyendinhlinh_final_basicandroid

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    val mReceiver = object :BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            val num  = intent!!.getLongExtra("num",0)
            Dialog(num).show(supportFragmentManager, Dialog::class.java.name)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startService(Intent(this, CalculatorService::class.java))
    }

    override fun onResume() {
        super.onResume()
        val intentFilter: IntentFilter = IntentFilter().apply {
            addAction("done")
        }
        registerReceiver(mReceiver, intentFilter)
    }
    override fun onDestroy() {
        super.onDestroy()
        stopService(Intent(this, CalculatorService::class.java))
    }
}